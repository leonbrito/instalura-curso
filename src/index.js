import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Login from './components/Login';
import Logout from './components/Logout';
import {BrowserRouter as Router, Route, Switch, Redirect, matchPath} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import './css/reset.css';
import './css/timeline.css';
import './css/login.css';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {timeline} from './reducers/timeline';
import {notificacao} from './reducers/header';
import {Provider} from 'react-redux';

const reducers = combineReducers({timeline, notificacao})
const store = createStore(reducers, applyMiddleware(thunkMiddleware));

function verificaAutenticacao(component){
    const history = createBrowserHistory();
    const match = matchPath(history.location.pathname,  {path: '/timeline/:login'});
    const privateRoute = match === null;

    if(privateRoute && localStorage.getItem('auth-token') === null){
        return <Redirect to={{pathname: '/', state:{msg:'usuário não autenticado'}}}/>
    } else{
        return component;
    }

}

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Switch> 
                <Route exact path="/" component={Login}/>            
                <Route path='/timeline/:login?' 
                    render={(props) => { return verificaAutenticacao(<App {...props}/>) }}/>
                <Route path="/logout" component={Logout}/>
            </Switch>
        </Router>
    </Provider>, document.getElementById('root'));
registerServiceWorker();


// render={() => ( 
//     verificaAutenticacao() ? (<Redirect to={{
//         pathname: '/', 
//         state:{msg:'usuário não autenticado'}
//     }} />) : (<App />))}